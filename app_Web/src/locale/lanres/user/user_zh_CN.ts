export default {
  fields: {
    password: 'password',
    address: 'address',
    weixin: 'weixin',
    dingding: 'dingding',
    fails: 'fails',
    slack: 'slack',
    ranzhi: 'ranzhi',
    account: 'account',
    locked: 'locked',
    avatar: 'avatar',
    scorelevel: 'scoreLevel',
    realname: 'realname',
    zipcode: 'zipcode',
    dept: 'dept',
    commiter: 'commiter',
    role: 'role',
    deleted: '逻辑删除标志',
    last: 'last',
    clientstatus: 'clientStatus',
    skype: 'skype',
    whatsapp: 'whatsapp',
    score: 'score',
    gender: 'gender',
    mobile: 'mobile',
    clientlang: 'clientLang',
    visits: 'visits',
    join: 'join',
    email: 'email',
    ip: 'ip',
    birthday: 'birthday',
    nickname: 'nickname',
    phone: 'phone',
    id: 'id',
    qq: 'qq',
  },
};