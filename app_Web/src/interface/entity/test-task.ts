/**
 * 测试版本
 *
 * @export
 * @interface TestTask
 */
export interface TestTask {

    /**
     * 结束日期
     *
     * @returns {*}
     * @memberof TestTask
     */
    end?: any;

    /**
     * 开始日期
     *
     * @returns {*}
     * @memberof TestTask
     */
    begin?: any;

    /**
     * 抄送给
     *
     * @returns {*}
     * @memberof TestTask
     */
    mailto?: any;

    /**
     * 优先级
     *
     * @returns {*}
     * @memberof TestTask
     */
    pri?: any;

    /**
     * 子状态
     *
     * @returns {*}
     * @memberof TestTask
     */
    substatus?: any;

    /**
     * report
     *
     * @returns {*}
     * @memberof TestTask
     */
    report?: any;

    /**
     * 描述
     *
     * @returns {*}
     * @memberof TestTask
     */
    desc?: any;

    /**
     * 编号
     *
     * @returns {*}
     * @memberof TestTask
     */
    id?: any;

    /**
     * 当前状态
     *
     * @returns {*}
     * @memberof TestTask
     */
    status?: any;

    /**
     * 负责人
     *
     * @returns {*}
     * @memberof TestTask
     */
    owner?: any;

    /**
     * 已删除
     *
     * @returns {*}
     * @memberof TestTask
     */
    deleted?: any;

    /**
     * auto
     *
     * @returns {*}
     * @memberof TestTask
     */
    auto?: any;

    /**
     * 名称
     *
     * @returns {*}
     * @memberof TestTask
     */
    name?: any;

    /**
     * 所属产品
     *
     * @returns {*}
     * @memberof TestTask
     */
    product?: any;

    /**
     * 版本
     *
     * @returns {*}
     * @memberof TestTask
     */
    build?: any;

    /**
     * 所属项目
     *
     * @returns {*}
     * @memberof TestTask
     */
    project?: any;
}