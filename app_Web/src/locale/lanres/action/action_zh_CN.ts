export default {
  fields: {
    extra: '附加值',
    objecttype: '对象类型',
    id: 'id',
    comment: '备注',
    read: '已读',
    action: '动作',
    date: '日期',
    product: '产品',
    objectid: '对象ID',
    actor: '操作者',
    project: '项目',
  },
	views: {
		histroylistview: {
			caption: '系统日志',
      title: '系统日志',
		},
		projecttrendslistview: {
			caption: '系统日志',
      title: '系统日志',
		},
		projecttrendslistview9: {
			caption: '系统日志',
      title: '系统日志',
		},
		producttrendslistview9: {
			caption: '系统日志',
      title: '系统日志',
		},
		producttrendslistview: {
			caption: '系统日志',
      title: '系统日志',
		},
		editview: {
			caption: '系统日志',
      title: '系统日志',
		},
	},
	main_form: {
		details: {
			group1: 'action基本信息', 
			formpage1: '基本信息', 
			group2: '操作信息', 
			formpage2: '其它', 
			srforikey: '', 
			srfkey: 'id', 
			srfmajortext: '备注', 
			srftempmode: '', 
			srfuf: '', 
			srfdeid: '', 
			srfsourcekey: '', 
			id: 'id', 
		},
		uiactions: {
		},
	},
	editviewtoolbar_toolbar: {
		tbitem3: {
			caption: '保存',
			tip: '保存',
		},
		tbitem4: {
			caption: '保存并新建',
			tip: '保存并新建',
		},
		tbitem5: {
			caption: '保存并关闭',
			tip: '保存并关闭',
		},
		tbitem6: {
			caption: '-',
			tip: '',
		},
		tbitem7: {
			caption: '删除并关闭',
			tip: '删除并关闭',
		},
		tbitem8: {
			caption: '-',
			tip: '',
		},
		tbitem12: {
			caption: '新建',
			tip: '新建',
		},
		tbitem13: {
			caption: '-',
			tip: '',
		},
		tbitem14: {
			caption: '拷贝',
			tip: '拷贝',
		},
		tbitem16: {
			caption: '-',
			tip: '',
		},
		tbitem23: {
			caption: '第一个记录',
			tip: '第一个记录',
		},
		tbitem24: {
			caption: '上一个记录',
			tip: '上一个记录',
		},
		tbitem25: {
			caption: '下一个记录',
			tip: '下一个记录',
		},
		tbitem26: {
			caption: '最后一个记录',
			tip: '最后一个记录',
		},
		tbitem21: {
			caption: '-',
			tip: '',
		},
		tbitem22: {
			caption: '帮助',
			tip: '帮助',
		},
	},
};