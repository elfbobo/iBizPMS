/**
 * Db_container1 部件模型
 *
 * @export
 * @class Db_container1Model
 */
export default class Db_container1Model {

  /**
    * 获取数据项集合
    *
    * @returns {any[]}
    * @memberof Db_container1Model
    */
  public getDataItems(): any[] {
    return [
    ]
  }


}