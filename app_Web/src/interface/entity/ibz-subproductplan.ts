/**
 * 产品计划
 *
 * @export
 * @interface IBZ_SUBPRODUCTPLAN
 */
export interface IBZ_SUBPRODUCTPLAN {

    /**
     * 名称
     *
     * @returns {*}
     * @memberof IBZ_SUBPRODUCTPLAN
     */
    title?: any;

    /**
     * 编号
     *
     * @returns {*}
     * @memberof IBZ_SUBPRODUCTPLAN
     */
    id?: any;

    /**
     * 开始日期
     *
     * @returns {*}
     * @memberof IBZ_SUBPRODUCTPLAN
     */
    begin?: any;

    /**
     * 描述
     *
     * @returns {*}
     * @memberof IBZ_SUBPRODUCTPLAN
     */
    desc?: any;

    /**
     * 结束日期
     *
     * @returns {*}
     * @memberof IBZ_SUBPRODUCTPLAN
     */
    end?: any;

    /**
     * 已删除
     *
     * @returns {*}
     * @memberof IBZ_SUBPRODUCTPLAN
     */
    deleted?: any;

    /**
     * 排序
     *
     * @returns {*}
     * @memberof IBZ_SUBPRODUCTPLAN
     */
    order?: any;

    /**
     * 父计划名称
     *
     * @returns {*}
     * @memberof IBZ_SUBPRODUCTPLAN
     */
    parentname?: any;

    /**
     * 平台/分支
     *
     * @returns {*}
     * @memberof IBZ_SUBPRODUCTPLAN
     */
    branch?: any;

    /**
     * 父计划
     *
     * @returns {*}
     * @memberof IBZ_SUBPRODUCTPLAN
     */
    parent?: any;

    /**
     * 产品
     *
     * @returns {*}
     * @memberof IBZ_SUBPRODUCTPLAN
     */
    product?: any;
}