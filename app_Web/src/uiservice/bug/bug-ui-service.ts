import BugUIServiceBase from './bug-ui-service-base';

/**
 * BugUI服务对象
 *
 * @export
 * @class BugUIService
 */
export default class BugUIService extends BugUIServiceBase {

    /**
     * Creates an instance of  BugUIService.
     * 
     * @param {*} [opts={}]
     * @memberof  BugUIService
     */
    constructor(opts: any = {}) {
        super(opts);
    }

}