package cn.ibizlab.pms.core.zentao.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.Collection;
import java.math.BigInteger;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import com.alibaba.fastjson.JSONObject;
import org.springframework.cache.annotation.CacheEvict;

import cn.ibizlab.pms.core.zentao.domain.GroupPriv;
import cn.ibizlab.pms.core.zentao.filter.GroupPrivSearchContext;


import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 实体[GroupPriv] 服务对象接口
 */
public interface IGroupPrivService extends IService<GroupPriv>{

    boolean create(GroupPriv et) ;
    void createBatch(List<GroupPriv> list) ;
    boolean save(GroupPriv et) ;
    void saveBatch(List<GroupPriv> list) ;
    GroupPriv get(String key) ;
    boolean remove(String key) ;
    void removeBatch(Collection<String> idList) ;
    boolean checkKey(GroupPriv et) ;
    boolean update(GroupPriv et) ;
    void updateBatch(List<GroupPriv> list) ;
    GroupPriv getDraft(GroupPriv et) ;
    Page<GroupPriv> searchDefault(GroupPrivSearchContext context) ;
    /**
     *自定义查询SQL
     * @param sql  select * from table where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return select * from table where id = '1'
     */
    List<JSONObject> select(String sql, Map param);
    /**
     *自定义SQL
     * @param sql  update table  set name ='test' where id =#{et.param}
     * @param param 参数列表  param.put("param","1");
     * @return     update table  set name ='test' where id = '1'
     */
    boolean execute(String sql, Map param);


}


