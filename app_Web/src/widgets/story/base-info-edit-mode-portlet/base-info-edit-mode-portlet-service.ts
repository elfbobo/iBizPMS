import { Http } from '@/utils';
import ControlService from '@/widgets/control-service';

/**
 * BaseInfo_EditMode 部件服务对象
 *
 * @export
 * @class BaseInfo_EditModeService
 */
export default class BaseInfo_EditModeService extends ControlService {
}